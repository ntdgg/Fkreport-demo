
<div align="center">
<br/>
<br/>
  <h1 align="center">
    方块报表 v1.0
  </h1>
  <h4 align="center">
    致 力 于 企 业 信 息 化 解 决 方 案
  </h4> 

[预 览](http://tpflow.gadmin8.com)   |   [官 网](http://www.gadmin8.com/)   |   [群聊](https://jq.qq.com/?_wv=1027&k=uIJZE54F) |   [文档](https://gadmin8.com/index/product.html) |   [Gadmin产品](http://gadmin8.com)
</div>
<div align="center">

[![GitHub stars](https://img.shields.io/badge/license-Mit-yellowgreen?style=flat-square&logo=github)](https://gitee.com/ntdgg/sfdp)
[![GitHub forks](https://img.shields.io/badge/Sfdp-6.0-brightgreen?style=flat-square&logo=github)](https://gitee.com/ntdgg/sfdp)
[![GitHub license](https://img.shields.io/badge/Language-PHP8-orange?style=flat-square&logo=)](https://gitee.com/ntdgg/tpflow)

</div>

- Fkreport 方块报表继承了`<Tpflow>` `<SFDP>` 一贯作风，秉承极简、高效的开发方式，站在企业开发的角度，极易集成，极易使用。
- Fkreport 方块报表 致力于企业级的大屏设计，下阶段也将进入报表开发领域。
- 欢迎各位大佬加入组织，共同努力！

```
报表仍处于高开发阶段，欢迎各位铁子们加入开发！
点个Star支持下我们吧！！
```

# ⭐️ 简单使用说明
1、git clone https://gitee.com/ntdgg/Fkreport.git

2、composer update

3、导入数据库 fkreport.sql

4、完成

5、遇到问题 [issue](https://gitee.com/ntdgg/Fkreport/issues)

# ⭐️ 部分功能截图

演示站点：http://fkreport.com

<p align="center">
<img src="https://www.gadmin8.com/img/1.png" width="500" />
</p>
<p align="center">
<img src="https://www.gadmin8.com/img/2.png" width="500" />
</p>

# ✈️主要特性

+ 基于 可视化设计大屏设计器
    + 支持可视化界面设计
    + 支持拖拽式绘制
    + 自动化适应屏幕大小
    + 极易继承，轻松拥有大屏

+ `<G2>`丰富组件功能
    +  饼状图
    +  柱状图
    +  折线图
    +  面积图
    +  玫瑰图
    +  仪表盘
    +  水波图
    +  时间时钟
    +  大屏图片

# ❤  [流之云科技](https://liuzhiyun.com) —— 旗下作品

| 名称                                         | 说明             |
  | -------------------------------------------- | ---------------- |
| [Tpflow](https://gitee.com/ntdgg/tpflow)     | PHP 工作流引擎   |
| [SFDP](https://gitee.com/ntdgg/sfdp)         | PHP 超级表单     |
| [Fkreport](https://gitee.com/ntdgg/Fkreport) | PHP 报表开发平台 |
| [Gadmin](https://gadmin8.com)                | 企业级开发平台   |

# ☀️ 流之云科技 —— 青年计划

流之云科技一直致力于中国 PHP 工作流引擎、业务表单引擎、报表引擎的研发和设计，至今已经有 5 个年头，2018 年初正式立项研发工作流引擎，2019 年获得开源中国 GVP 项目。TPFLOW、SFDP、Fkreport 全部使用最宽松的开源 MIT 协议（可以应用与商业系统、个人系统均可，只需保留版权信息）;[使用声明](https://www.cojz8.com/topic/2)

青年计划，为了 PHP 开源工作流引擎、业务表单引擎的进步，我们全面开启青年计划，服务更多青年学者，为开源做奉献，为工作流引擎做奉献！[前去申请](https://gadmin8.com/index/young.html)

# ⭐ 流之云科技 —— 交流群

交流群 ①：532797225 （已满）

交流群 ②：778657302

Vip 交流群：1062040103 加群条件：[申请加群](https://www.cojz8.com/article/148 "加群条件")

手册：https://gadmin8.com/index/product.html 【付费】

# ⌛ 版权及严正声明

```
设计器、源代码标注有版权的位置，未经许可，严禁删除及修改，违者将承担法律侵权责任！
坚决打击网络诈骗行为，严禁将本插件集成在任何违法违规的程序上。
```

# ✨ 鸣谢 Thanks

- 感谢 [JetBrains](https://www.jetbrains.com) 提供生产力巨高的 `PHPStorm`和`WebStorm`
  > 排名不分先后

# ㊙️ 链接传送门

# ©️ 版权信息

Fkreport 遵循 MIT 开源协议发布，并提供免费使用。

使用本项目必须保留所有版权信息。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有 Copyright © 2022-2025 by Fkreport (http://liuzhiyun.com)

All rights reserved。

```
对您有帮助的话，你可以在下方赞助我们，让我们更好的维护开发，谢谢！
特别声明：坚决打击网络诈骗行为，严禁将本插件集成在任何违法违规的程序上。
```
